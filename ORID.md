# day06-ORID

## O

a. code review. The teacher pointed out the code smell in my code. Many repetitive parts can extract an abstract class to reduce the coupling degree of the code.

b. teamwork presentation. We not only studied Strategy Pattern, but also knowed Command Pattern and Observer Pattern through other team's presentation.

c. We Studied refactoring, knowed the meaning of refactoring and was familiar with the process of implementing refactoring through practice.

## R

I was fruitful.

## I

Why learned refactoring? Refactoring can make code cleaner and easier to understand.

## D

While refactoring can make code cleaner and easier to understand, it's best to focus on formatting during development and avoid code smell.  And I was still not familiar with Command Pattern and Observer Pattern, and it is necessary to further deepen the learning of Observer Pattern and Command Pattern by consulting data.