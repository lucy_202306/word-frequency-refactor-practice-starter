import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static String split_Regex = "\\s+";
    public static String ERROR_MESSAGE = "Calculate Error";
    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencyList = getWordFrequencyList(inputStr);
            List<WordFrequency> wordFrequencyListByWord = getWordFrequencyListByWord(wordFrequencyList);
            List<WordFrequency> sortedList = getSortedWordFrequencyList(wordFrequencyListByWord);
            return getStringJoiner(sortedList);
        } catch (Exception e) {
            return ERROR_MESSAGE;
        }
    }

    private static List<WordFrequency> getSortedWordFrequencyList(List<WordFrequency> wordFrequencyListByWord) {
        wordFrequencyListByWord.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
        return wordFrequencyListByWord;
    }

    private static List<WordFrequency> getWordFrequencyList(String inputStr) {
        return Arrays.stream(inputStr.split(split_Regex))
                .map(input -> new WordFrequency(input, 1))
                .collect(Collectors.toList());
    }

    private static String getStringJoiner(List<WordFrequency> wordFrequencyList) {
        StringJoiner joiner = new StringJoiner("\n");
        wordFrequencyList.stream().map(wordFrequency -> wordFrequency.getWord() + " " + wordFrequency.getWordCount()).forEach(joiner::add);
        return joiner.toString();
    }

    private List<WordFrequency> getWordFrequencyListByWord(List<WordFrequency> wordFrequencyList) {
        return getWordFrequencyListMap(wordFrequencyList).entrySet()
                .stream()
                .map(entry -> new WordFrequency(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toList());
    }

    private Map<String, List<WordFrequency>> getWordFrequencyListMap(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> wordFrequencyListMap = new HashMap<>();
        for (WordFrequency wordFrequency : wordFrequencyList) {
            if (!wordFrequencyListMap.containsKey(wordFrequency.getWord())) {
                ArrayList arr = new ArrayList<>();
                arr.add(wordFrequency);
                wordFrequencyListMap.put(wordFrequency.getWord(), arr);
            } else {
                wordFrequencyListMap.get(wordFrequency.getWord()).add(wordFrequency);
            }
        }
        return wordFrequencyListMap;
    }


}
